﻿using LevvaCoinsAPI.Models;
using System.Data.Entity;

namespace LevvaCoinsAPI.Database
{
    public class LevvaCoinsDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}